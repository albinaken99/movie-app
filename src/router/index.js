import { createRouter, createWebHistory } from "vue-router";
import MovieDetails from "../views/MovieDetails.vue";
import MovieList from "../views/MovieList.vue";
import SearchForm from "../views/SearchForm.vue";

const routes = [
  {
    path: "/",
    name: "MovieList",
    component: MovieList,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
  },
  {
    path: "/movies",
    name: "movies",
    component: SearchForm,
  },
  {
    path: "/movies/:id",
    name: "moviesId",
    component: MovieDetails,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
